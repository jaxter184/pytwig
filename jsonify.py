#!/usr/bin/env python3
from pytwig import bw_file
import os, sys

# usage: `./jsonify.py name-of-file.bwpreset`

read = bw_file.BW_File()
read.read(sys.argv[1])
print(read.serialize())
