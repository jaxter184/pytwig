#!/usr/bin/env python3
from pytwig import bw_file
import os, sys

# usage: `./jsonify-collection.py name-of-file.scollection`

read = bw_file.BW_File()
read.read(sys.argv[1], skip_meta = True)
print(read.serialize())
