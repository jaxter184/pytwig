#!/usr/bin/env python3
from pytwig import bw_file as f
from pytwig import bw_preset as p
from pytwig import bw_object as o

# This script creates a clip from scratch

if __name__ == "__main__":
	# Create and initialize a BW_File object
	test = p.BW_Preset_File()
	test.set_header('BtWg00010002008c000015120000000000000000')

	# Set to drum machine preset (this should be more ergonomic later)
	preset = o.BW_Object(64) # native_device_preset(64)
	preset.set(153, "8ea97e45-0255-40fd-bc7e-94419741e9d1") # device_UUID(153)
	preset.set(154, "Drum Machine") # device_name(154)
	preset.set(155, "Bitwig") # device_creator(155)
	preset.set(156, "Container") # device_category(156)
	preset.set(157, 2) # device_type(157)
	preset.set(158, "jaxter184") # creator(158)
	preset.set(163, True) # enabled(163)
	preset.set(4990, True) # active(4990)
	preset.set(4830, "test") # preset_name(4830)

	contents = o.BW_Object(211)
	contents.set(697, "CONTENTS") # identifier(697)

	value = o.BW_Object(133)
	value.set(697, "MASTER_VOLUME") # identifier(697)
	value.set(310, 0.5) # value(310)
	contents.set(524, [
		value
	])
	preset.set(164, contents)

	test.set_preset(preset)
	test.write('pytwigtest.bwpreset')
	test.export('pytwigtest.json')
