#!/usr/bin/env python3
from pytwig import bw_file as f
from pytwig import bw_clip as c

# This script creates a clip from scratch

if __name__ == "__main__":
	# Create a BW_File object, then populate its contents by reading from the file
	ppcrn = f.BW_Clip_File()
	ppcrn.set_header('BtWg00010002008c000015120000000000000000')

	# A clip file is basically a project file that contains a main track
	# BW_Clip_File makes this main track for us automatically, but
	# not the clip, as any kind of clip could be put in the track.
	clip = c.Note_Clip(16)
	# We could also get the main track from the clip file, then call
	# create_note_clip() from the track object

	# Setting some variables so we can easily pitch shift and elongate in the future
	root_note = 67
	duration = 0.1

	# This is where the actual notes are made. Many of the objects have helper
	# functions for creating sub-objects. We could have done a similar thing above
	# to create the note clip.
	for i in range(2):
		offset = float(i*4)
#		clip.create_note(pitch,        start time,   duration)
		clip.create_note(root_note+12, offset + 0.0, duration)
		clip.create_note(root_note+10, offset + 0.5, duration)
		clip.create_note(root_note+12, offset + 1.0, duration)
		clip.create_note(root_note+ 7, offset + 1.5, duration)
		clip.create_note(root_note+ 3, offset + 2.0, duration)
		clip.create_note(root_note+ 7, offset + 2.5, duration)
		clip.create_note(root_note+ 0, offset + 3.0, duration)

	clip.create_note(root_note+12,  8.0, duration)
	for i in range(2):
		offset = float(i)
		clip.create_note(root_note+14, offset +  8.5, duration)
		clip.create_note(root_note+15, offset +  9.0, duration)
		clip.create_note(root_note+12, offset + 10.5, duration)
		clip.create_note(root_note+14, offset + 11.0, duration)
		clip.create_note(root_note+10, offset + 12.5, duration)
		clip.create_note(root_note+12, offset + 13.0, duration)
	clip.create_note(root_note+ 8, 14.5, duration)
	clip.create_note(root_note+12, 15.0, duration)

	#clip.set_loop(True, 5)
	#clip.set_duration(16)
	#clip.get(648).get(1180).get(6347)[-1].set(238, 60)

	# Add the note clip to the clip file's main track
	ppcrn.get_main_track().set_main_clip(clip)

	# Write the file in both human-readable json and compressed bytecode formats
	# The json version cannot be read by Bitwig, but I've included it to help
	# the user better understand the structure of clip files.
	ppcrn.export('ppcrn json.bwclip')
	ppcrn.write('ppcrn.bwclip')
