#!/usr/bin/env python3

# OSC stream parsing, should belong in a separate module, but im lazy
class OSC_Stream():
	stream = []
	def parse(self, string):
		current_bundle = self.stream
		for ea_line in string.split('\n'): #TODO: accommodate for crl
			indented = False
			if not (ea_line):
				continue
			while ea_line[0] in ('\t', ' '):
				ea_line = ea_line[1:]
				indented = True
			tokens = ea_line.split(' ')
			if ea_line[0:2] in ('# ', '##'):
				continue # skip line
			if tokens[0] == '#bundle':
				# parse bundle
				whole, fractional = [int(i, 0) for i in tokens[1].split('.')]
				time = float(whole) + float(fractional)/0x100000000
				current_bundle = []
				self.stream.append({'time': time, 'content': current_bundle})
			elif tokens[0][0] == '/':
				# parse as osc message
				address, typetag = tokens[0].split(',')
				message = {'address':address, 'typetag':typetag, 'bytes':tokens[1:]}
				current_bundle.append(message)
			else:
				raise SyntaxError("Expected '/' or '#bundle', got {}".format(tokens[0]))

	def read(self, filename):
		with open(filename, 'r') as f:
			self.parse(f.read())

	def get_stream(self):
		return self.stream

from pytwig import bw_file as f

# This script converts a .oscs file to a .bwclip file

if __name__ == "__main__":
	oscstream = OSC_Stream()
	oscstream.read('input/test.oscs')
	print(oscstream.get_stream())
	notes = []
	# Create a BW_File object, then populate its contents by reading from the file
#	clip_file = f.BW_Clip_File()
#	clip_file.set_header('BtWg00010002008c000015120000000000000000')
#
#	# Setting some variables so we can easily pitch shift and elongate in the future
#	root_note = 67
#	duration = 0.1
#
#	#clip.set_loop(True, 5)
#	#clip.set_duration(16)
#	#clip.get(648).get(1180).get(6347)[-1].set(238, 60)
#
#	# Add the note clip to the clip file's main track
#	clip_file.get_main_track().set_main_clip(clip)
#
#	# Write the file in both human-readable json and compressed bytecode formats
#	# The json version cannot be read by Bitwig, but I've included it to help
#	# the user better understand the structure of clip files.
#	clip_file.export('converted json.bwclip')
#	clip_file.write('converted.bwclip')
